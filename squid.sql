-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2023 a las 16:55:35
-- Versión del servidor: 10.4.28-MariaDB
-- Versión de PHP: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `squid`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aprobacion`
--

CREATE TABLE `aprobacion` (
  `id_apro` int(11) NOT NULL,
  `nombre_apro` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cache_squid`
--

CREATE TABLE `cache_squid` (
  `id_cache` int(11) NOT NULL,
  `nombre_cache` varchar(50) DEFAULT NULL,
  `capacidad_mb` int(11) DEFAULT NULL,
  `ubicacion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion_squid`
--

CREATE TABLE `configuracion_squid` (
  `id_config` int(11) NOT NULL,
  `nombre_config` varchar(50) DEFAULT NULL,
  `valor_config` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `id_hor` int(11) NOT NULL,
  `nombre_hor` varchar(50) DEFAULT NULL,
  `hora_hor` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ip`
--

CREATE TABLE `ip` (
  `id_ip` int(11) NOT NULL,
  `direccion_ip` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paginas`
--

CREATE TABLE `paginas` (
  `id_pag` int(11) NOT NULL,
  `nombre_pag` varchar(50) DEFAULT NULL,
  `id_apro` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `nombre_rol` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE `solicitud` (
  `id_sol` int(11) NOT NULL,
  `nombre_sol_apellido_sol` varchar(100) DEFAULT NULL,
  `correo_sol` varchar(50) DEFAULT NULL,
  `nacimiento_sol` date DEFAULT NULL,
  `contrasenia_sol` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usu` int(11) NOT NULL,
  `nombre_usu` varchar(50) DEFAULT NULL,
  `apellido_usu` varchar(50) DEFAULT NULL,
  `correo_usu` varchar(50) DEFAULT NULL,
  `contrasenia_usu` varchar(50) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL,
  `id_pag` int(11) DEFAULT NULL,
  `id_hor` int(11) DEFAULT NULL,
  `id_sol` int(11) DEFAULT NULL,
  `id_ip` int(11) DEFAULT NULL,
  `id_config` int(11) DEFAULT NULL,
  `id_cache` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aprobacion`
--
ALTER TABLE `aprobacion`
  ADD PRIMARY KEY (`id_apro`);

--
-- Indices de la tabla `cache_squid`
--
ALTER TABLE `cache_squid`
  ADD PRIMARY KEY (`id_cache`);

--
-- Indices de la tabla `configuracion_squid`
--
ALTER TABLE `configuracion_squid`
  ADD PRIMARY KEY (`id_config`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`id_hor`);

--
-- Indices de la tabla `ip`
--
ALTER TABLE `ip`
  ADD PRIMARY KEY (`id_ip`);

--
-- Indices de la tabla `paginas`
--
ALTER TABLE `paginas`
  ADD PRIMARY KEY (`id_pag`),
  ADD KEY `id_apro` (`id_apro`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`id_sol`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usu`),
  ADD KEY `id_rol` (`id_rol`),
  ADD KEY `id_pag` (`id_pag`),
  ADD KEY `id_hor` (`id_hor`),
  ADD KEY `fk_solicitud` (`id_sol`),
  ADD KEY `fk_ip` (`id_ip`),
  ADD KEY `fk_configuracion_squid` (`id_config`),
  ADD KEY `fk_cache_squid` (`id_cache`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `paginas`
--
ALTER TABLE `paginas`
  ADD CONSTRAINT `paginas_ibfk_1` FOREIGN KEY (`id_apro`) REFERENCES `aprobacion` (`id_apro`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_cache_squid` FOREIGN KEY (`id_cache`) REFERENCES `cache_squid` (`id_cache`),
  ADD CONSTRAINT `fk_configuracion_squid` FOREIGN KEY (`id_config`) REFERENCES `configuracion_squid` (`id_config`),
  ADD CONSTRAINT `fk_ip` FOREIGN KEY (`id_ip`) REFERENCES `ip` (`id_ip`),
  ADD CONSTRAINT `fk_solicitud` FOREIGN KEY (`id_sol`) REFERENCES `solicitud` (`id_sol`),
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`),
  ADD CONSTRAINT `usuario_ibfk_2` FOREIGN KEY (`id_pag`) REFERENCES `paginas` (`id_pag`),
  ADD CONSTRAINT `usuario_ibfk_3` FOREIGN KEY (`id_hor`) REFERENCES `horario` (`id_hor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
